# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-08 04:04+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../epour/gui/Preferences.py:98
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:98
msgid "Epour General Preferences"
msgstr ""

#: ../epour/gui/Preferences.py:112
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:112
msgid "Move completed torrents"
msgstr ""

#: ../epour/gui/Preferences.py:120
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:120
msgid "Completed torrents"
msgstr ""

#: ../epour/gui/Preferences.py:149
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:149
msgid "Delete original .torrent file when added"
msgstr ""

#: ../epour/gui/Preferences.py:157
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:157
msgid "Ask for confirmation on exit"
msgstr ""

#: ../epour/gui/Preferences.py:166
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:166
msgid "Torrents to be added from dbus or command line open a dialog"
msgstr ""

#: ../epour/gui/Preferences.py:201
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:201
msgid "Change path"
msgstr ""

#: ../epour/gui/Preferences.py:238
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:238
msgid "Listen port (range)"
msgstr ""

#: ../epour/gui/Preferences.py:277
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:277
msgid "Epour Proxy Preferences"
msgstr ""

#: ../epour/gui/Preferences.py:280
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:280
msgid "Proxy for torrent peer connections"
msgstr ""

#: ../epour/gui/Preferences.py:282
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:282
msgid "Proxy for torrent web seed connections"
msgstr ""

#: ../epour/gui/Preferences.py:284
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:284
msgid "Proxy for tracker connections"
msgstr ""

#: ../epour/gui/Preferences.py:286
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:286
msgid "Proxy for DHT connections"
msgstr ""

#: ../epour/gui/Preferences.py:322
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:322
msgid "Proxy type"
msgstr ""

#: ../epour/gui/Preferences.py:332
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:332
msgid "Hostname"
msgstr ""

#: ../epour/gui/Preferences.py:343
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:343
msgid "Port"
msgstr ""

#: ../epour/gui/Preferences.py:351
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:351
msgid "Username"
msgstr ""

#: ../epour/gui/Preferences.py:362
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:362
msgid "Password"
msgstr ""

#: ../epour/gui/Preferences.py:374
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:374
msgid "Proxy hostname lookups"
msgstr ""

#: ../epour/gui/Preferences.py:383
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:383
msgid "Proxy peer connections"
msgstr ""

#: ../epour/gui/Preferences.py:393 ../epour/gui/Preferences.py:461
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:393
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:461
msgid "Apply"
msgstr ""

#: ../epour/gui/Preferences.py:412
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:412
#, python-format
msgid "%s settings saved"
msgstr ""

#: ../epour/gui/Preferences.py:424
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:424
msgid "Encryption settings"
msgstr ""

#: ../epour/gui/Preferences.py:436
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:436
msgid "Incoming encryption"
msgstr ""

#: ../epour/gui/Preferences.py:442
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:442
msgid "Outgoing encryption"
msgstr ""

#: ../epour/gui/Preferences.py:448
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:448
msgid "Allowed encryption level"
msgstr ""

#: ../epour/gui/Preferences.py:455
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:455
msgid "Prefer RC4 ecryption"
msgstr ""

#: ../epour/gui/Preferences.py:485
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Preferences.py:485
msgid "Epour Session Preferences"
msgstr ""

#: ../epour/gui/Widgets.py:130
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Widgets.py:130
msgid "Close"
msgstr ""

#: ../epour/gui/Widgets.py:143
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Widgets.py:143
msgid "OK"
msgstr ""

#: ../epour/gui/Widgets.py:152
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Widgets.py:152
msgid "Confirm exit"
msgstr ""

#: ../epour/gui/Widgets.py:153
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Widgets.py:153
msgid "Are you sure you wish to exit Epour?"
msgstr ""

#: ../epour/gui/Widgets.py:155
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Widgets.py:155
msgid "Yes"
msgstr ""

#: ../epour/gui/Widgets.py:159
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/Widgets.py:159
msgid "No"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:122
#: ../epour/gui/__init__.py:122
msgid "Add torrent"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:134
#: ../epour/gui/__init__.py:134
msgid "Pause Session"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:137
#: ../epour/gui/__init__.py:137
msgid "Resume Session"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:152
#: ../epour/gui/__init__.py:152
msgid "Preferences"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:155
#: ../epour/gui/__init__.py:155
msgid "General"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:158
#: ../epour/gui/__init__.py:158
msgid "Proxy"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:161
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:386
#: ../epour/gui/__init__.py:161 ../epour/gui/__init__.py:386
msgid "Session"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:164
#: ../epour/gui/__init__.py:164
msgid "Exit"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:287
#: ../epour/gui/__init__.py:287
msgid "Torrent {} has finished downloading."
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:417
#: ../epour/gui/__init__.py:417
msgid "Peer connections"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:425
#: ../epour/gui/__init__.py:425
msgid "Upload slots"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:434
#: ../epour/gui/__init__.py:434
msgid "Listening"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:478
#: ../epour/gui/__init__.py:478
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:540
#: ../epour/gui/TorrentProps.py:540
msgid "Queued"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:478
#: ../epour/gui/__init__.py:478
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:540
#: ../epour/gui/TorrentProps.py:540
msgid "Checking"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:478
#: ../epour/gui/__init__.py:478
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:540
#: ../epour/gui/TorrentProps.py:540
msgid "Downloading metadata"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:479
#: ../epour/gui/__init__.py:479
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:540
#: ../epour/gui/TorrentProps.py:540
msgid "Downloading"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:479
#: ../epour/gui/__init__.py:479
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:541
#: ../epour/gui/TorrentProps.py:541
msgid "Finished"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:479
#: ../epour/gui/__init__.py:479
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:541
#: ../epour/gui/TorrentProps.py:541
msgid "Seeding"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:479
#: ../epour/gui/__init__.py:479
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:541
#: ../epour/gui/TorrentProps.py:541
msgid "Allocating"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:480
#: ../epour/gui/__init__.py:480
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:541
#: ../epour/gui/TorrentProps.py:541
msgid "Checking resume data"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:500
#: ../epour/gui/__init__.py:500
msgid "Invalid torrent"
msgstr ""

#. NOTE: Estimated Time of Arrival (time until the process is finished)
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:516
#: ../epour/gui/__init__.py:516
#, python-brace-format
msgid "ETA: {0} "
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:519
#: ../epour/gui/__init__.py:519
#, python-brace-format
msgid "Download: {0}/s "
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:521
#: ../epour/gui/__init__.py:521
#, python-brace-format
msgid "Upload: {0}/s "
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:523
#: ../epour/gui/__init__.py:523
#, python-brace-format
msgid "Queue position: {0}"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:611
#: ../epour/gui/__init__.py:611
msgid "Resume"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:611
#: ../epour/gui/__init__.py:611
msgid "Pause"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:616
#: ../epour/gui/__init__.py:616
msgid "Queue"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:618
#: ../epour/gui/__init__.py:618
msgid "Up"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:620
#: ../epour/gui/__init__.py:620
msgid "Down"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:622
#: ../epour/gui/__init__.py:622
msgid "Top"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:624
#: ../epour/gui/__init__.py:624
msgid "Bottom"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:626
#: ../epour/gui/__init__.py:626
msgid "Remove torrent"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:629
#: ../epour/gui/__init__.py:629
msgid "and data files"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:633
#: ../epour/gui/__init__.py:633
msgid "Force reannounce"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:644
#: ../epour/gui/__init__.py:644
msgid "Force DHT reannounce"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:647
#: ../epour/gui/__init__.py:647
msgid "Scrape tracker"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:655
#: ../epour/gui/__init__.py:655
msgid "Force re-check"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:669
#: ../epour/gui/__init__.py:669
msgid "Torrent properties"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:705
#: ../epour/gui/__init__.py:705
msgid "Time when added"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:706
#: ../epour/gui/__init__.py:706
msgid "Time when completed"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:707
#: ../epour/gui/__init__.py:707
msgid "All time downloaded"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:708
#: ../epour/gui/__init__.py:708
msgid "All time uploaded"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:709
#: ../epour/gui/__init__.py:709
msgid "Total wanted done"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:710
#: ../epour/gui/__init__.py:710
msgid "Total wanted"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:711
#: ../epour/gui/__init__.py:711
msgid "Total downloaded this session"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:713
#: ../epour/gui/__init__.py:713
msgid "Total uploaded this session"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:714
#: ../epour/gui/__init__.py:714
msgid "Total failed"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:715
#: ../epour/gui/__init__.py:715
msgid "Number of seeds"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:716
#: ../epour/gui/__init__.py:716
msgid "Number of peers"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:742
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:785
#: ../epour/gui/__init__.py:742 ../epour/gui/__init__.py:785
msgid "N/A"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/__init__.py:765
#: ../epour/gui/__init__.py:765
#, python-format
msgid "Pieces (scaled 1:%d)"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:60
#: ../epour/gui/TorrentSelector.py:60
msgid "Seed Mode"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:61
#: ../epour/gui/TorrentSelector.py:61
msgid "Share Mode"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:62
#: ../epour/gui/TorrentSelector.py:62
msgid "Apply IP Filter"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:63
#: ../epour/gui/TorrentSelector.py:63
msgid "Start Paused"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:64
#: ../epour/gui/TorrentSelector.py:64
msgid "Duplicate Is Error"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:65
#: ../epour/gui/TorrentSelector.py:65
msgid "Super Seeding"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:66
#: ../epour/gui/TorrentSelector.py:66
msgid "Sequential Download"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:73
#: ../epour/gui/TorrentSelector.py:73
msgid ""
"If Seed Mode is set, Epour will assume that all files are\n"
"present for this torrent and that they all match the hashes in the\n"
"torrent file. Each time a peer requests to download a block, the\n"
"piece is verified against the hash, unless it has been verified\n"
"already. If a hash fails, the torrent will automatically leave the\n"
"seed mode and recheck all the files. The use case for this mode is\n"
"if a torrent is created and seeded, or if the user already know\n"
"that the files are complete, this is a way to avoid the initial\n"
"file checks, and significantly reduce the startup time.\n"
"\n"
"Setting Seed Mode on a torrent without metadata (a .torrent\n"
"file) is a no-op and will be ignored."
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:89
#: ../epour/gui/TorrentSelector.py:89
#, python-format
msgid ""
"Determines if the torrent should be added in share mode or not.\n"
"Share mode indicates that we are not interested in downloading the\n"
"torrent, but merely want to improve our share ratio (i.e. increase\n"
"it). A torrent started in share mode will do its best to never\n"
"download more than it uploads to the swarm. If the swarm does not\n"
"have enough demand for upload capacity, the torrent will not\n"
"download anything. This mode is intended to be safe to add any\n"
"number of torrents to, without manual screening, without the risk\n"
"of downloading more than is uploaded.\n"
"\n"
"A torrent in share mode sets the priority to all pieces to 0,\n"
"except for the pieces that are downloaded, when pieces are decided\n"
"to be downloaded. This affects the progress bar, which might be set\n"
"to \"100% finished\" most of the time. Do not change file or piece\n"
"priorities for torrents in share mode, it will make it not work.\n"
"\n"
"The share mode has one setting, the share ratio target."
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:110
#: ../epour/gui/TorrentSelector.py:110
msgid ""
"Determines if the IP filter should apply to this torrent or not. By\n"
"default all torrents are subject to filtering by the IP filter\n"
"(i.e. this flag is set by default). This is useful if certain\n"
"torrents needs to be excempt for some reason, being an auto-update\n"
"torrent for instance."
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:119
#: ../epour/gui/TorrentSelector.py:119
msgid ""
"Specifies whether or not the torrent is to be started in a paused\n"
"state. I.e. it won't connect to the tracker or any of the peers\n"
"until it's resumed. This is typically a good way of avoiding race\n"
"conditions when setting configuration options on torrents before\n"
"starting them."
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:128
#: ../epour/gui/TorrentSelector.py:128
msgid ""
"Sets the torrent into super seeding mode. If the torrent is not a\n"
"seed, this flag has no effect."
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:134
#: ../epour/gui/TorrentSelector.py:134
msgid "Sets the sequential download state for the torrent."
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:141
#: ../epour/gui/TorrentSelector.py:141
msgid "Add Torrent"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:167
#: ../epour/gui/TorrentSelector.py:167
msgid "Enter torrent file path / magnet URI / info hash"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:175
#: ../epour/gui/TorrentSelector.py:175
msgid "Select file"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:182
#: ../epour/gui/TorrentSelector.py:182
msgid "Advanced Options"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:205
#: ../epour/gui/TorrentSelector.py:205
msgid "Name"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:206
#: ../epour/gui/TorrentSelector.py:206
msgid "Tracker ID"
msgstr ""

#. Downloaded data is saved in this path
#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:218
#: ../epour/gui/TorrentSelector.py:218
msgid "Data Save Path"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:263
#: ../epour/gui/TorrentSelector.py:263
msgid "Max Uploads"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:264
#: ../epour/gui/TorrentSelector.py:264
msgid "Max Connections"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:275
#: ../epour/gui/TorrentSelector.py:275
msgid "Upload Limit"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:276
#: ../epour/gui/TorrentSelector.py:276
msgid "Download Limit"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:301
#: ../epour/gui/TorrentSelector.py:301
msgid "Ok"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentSelector.py:305
#: ../epour/gui/TorrentSelector.py:305
msgid "Cancel"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:90
#: ../epour/gui/TorrentProps.py:90
msgid "Enable/disable file download"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:124
#: ../epour/gui/TorrentProps.py:124
msgid "Invalid torrent handle."
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:149
#: ../epour/gui/TorrentProps.py:149
msgid "Torrent info"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:157
#: ../epour/gui/TorrentProps.py:157
msgid "Torrent settings"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:165
#: ../epour/gui/TorrentProps.py:165
msgid "Torrent status"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:174
#: ../epour/gui/TorrentProps.py:174
msgid "Magnet URI"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:182
#: ../epour/gui/TorrentProps.py:182
msgid "Copy"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:210
#: ../epour/gui/TorrentProps.py:210
#, python-format
msgid "Epour - Files for torrent: %s"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:252
#: ../epour/gui/TorrentProps.py:252
msgid "Select all"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:256
#: ../epour/gui/TorrentProps.py:256
msgid "Select none"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:395
#: ../epour/gui/TorrentProps.py:395
msgid "Private"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:446
#: ../epour/gui/TorrentProps.py:446
msgid "Storage path"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:452
#: ../epour/gui/TorrentProps.py:452
msgid "Select"
msgstr ""

#: ../debian/epour/usr/lib/python2.7/dist-packages/epour/gui/TorrentProps.py:504
#: ../epour/gui/TorrentProps.py:504
msgid "disabled"
msgstr ""
